<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<br>

<form action="/order" method="post">
    <select name="dishes" multiple>
        <c:forEach items="${dishes}" var="dish">
            <option value="${dish}">${dish}</option>
        </c:forEach>
    </select>
    <input type="submit">
</form>
<a href="/history">order history</a><br>
</body>
</html>