package pl.sda.spring;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class Customer implements ICustomer {




    private String name;


    @Override
    public void setName(String name) {
        this.name = name;

    }


    @Override
    public String getName() {
        return name;
    }

 }
