package pl.sda.spring;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope("singleton")
public class Menu {
    private List<String> dishes = Arrays.asList("burger", "ziemniak", "ryba");

    public List<String> getDishes() {
        return dishes;
    }
}
