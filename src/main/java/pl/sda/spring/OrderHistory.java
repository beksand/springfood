package pl.sda.spring;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope("singleton")
public class OrderHistory {
    private Map<String, List<String>> orderHistory = new HashMap<>();

    public void addToHistory(String name, List<String> dishes){
        if (!orderHistory.containsKey(name)){
            orderHistory.put(name, new ArrayList<>());
        }
        orderHistory.get(name).addAll(dishes);
    }

    public Object getAllDishes() {
        List<String> all =  new ArrayList<>();
        for (Map.Entry<String, List<String>> s: orderHistory.entrySet()
             ) {
            all.addAll(s.getValue());
        }
        return all;
    }
}
