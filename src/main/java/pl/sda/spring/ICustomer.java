package pl.sda.spring;

public interface ICustomer {
    void setName(String name);
    String getName();

}
