package pl.sda.spring;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@org.springframework.stereotype.Controller
public class FoodController {

    @Autowired
    private ICustomer iCustomer;

    @Autowired
    private OrderHistory history;

    @Autowired
    private Menu menu;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView();
        if (iCustomer.getName()==null){
            modelAndView.setViewName("login");
        } else {
            prepareMakeOrderView(modelAndView);
        }

        return modelAndView;
    }

    private void prepareMakeOrderView(ModelAndView modelAndView) {
        modelAndView.setViewName("order");
        modelAndView.addObject("dishes", menu.getDishes());

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(@RequestParam("name") String name){
        iCustomer.setName(name);
        ModelAndView model = new ModelAndView();
        prepareMakeOrderView(model);

        return model;
    }
    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public ModelAndView dish(@RequestParam("dishes") List<String> dishes) {

        ModelAndView model = new ModelAndView();
        model.setViewName("confirmation");
        model.addObject("dish", dishes);
        history.addToHistory(iCustomer.getName(), dishes);
        return model;
    }
    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public ModelAndView history() {

        ModelAndView model = new ModelAndView("history");
        model.addObject("history", history.getAllDishes());
        return model;
    }
}
