package pl.sda.spring;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    public static final  int MAX_FILE_SIZE = 1024*1024*10;

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] {AppJavaConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return null;
    }
}
